# Eucaris Bridge Frontend

> Frontend app for that connects directly with EUCARIS

## Table of Contents

1. [Description](#Description)
2. [Getting started](#Getting-Started)
3. [Prerequisites](#Prerequisites)
4. [Installation](#Installation)
4. [Running the app](#Running-the-app)
5. [Licensing](#Licensing)

## Description

This app is a frontend application which shows an UI that allows the user to interact with `EUCARIS`.

## Getting started

In order to run the project it is needed to create a `.env` file locally.

Variables to fill:

- `NEXT_PUBLIC_OOTS_API`: OOTS - Eucaris Bridge URL (Backend)
- `NEXT_PUBLIC_MINIMAL_WEBAPP`: OOTS - Eucaris Bridge Frontend URL (minimal_webapp)

## Prerequisites

- [Node.js](https://nodejs.org/en/download/)

## Installation

For the installation of this project it is recommended to have [nvm](https://github.com/nvm-sh/nvm) installed. In case you don't want to install `nvm`, just make sure to use the `NodeJS` version that appears in file `.nvmrc`.

```bash
$ nvm use
$ yarn install
```

## Running the app

### Run the project locally

```bash
$ yarn dev
```

### Run with docker

```bash
$ docker-compose up --build
```

Open [http://localhost:3002](http://localhost:3002) with your browser to see the result.


## License

This software is licensed under [European Union Public License (EUPL) version 1.2.](https://code.europa.eu/oots/tdd/oots_ex/-/blob/main/LICENSE)


import List from '@/components/List';
import Table from '@/components/Table';
import clsx from 'clsx';
import Head from 'next/head';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { MoonLoader } from 'react-spinners';
import useSWR, { Fetcher, Key } from 'swr';
import Button from '../components/Button';
import styles from '../styles/Home.module.scss';

// TODO: Change any type to the correct type
const fetcher: Fetcher<any, Key> = (input: RequestInfo, init?: RequestInit) =>
  fetch(input, init).then((res) => res.json());

export default function Home() {
  const [state, setState] = useState(0);
  const [apiData, setApiData] = useState<any>();
  const [person, setPerson] = useState(0); //type of person (Natural or Legal)
  const [sessionId, setSessionId] = useState<string | null>('');
  const [vehicleIds, setVehicleIds] = useState<any>();
  const [vehicleIdsMarked, setVehicleIdsMarked] = useState<any>();
  const [userId, setUserId] = useState<any>();
  const [tableData, setTableData] = useState<any>();
  const [frontend1URL, setFrontend1URL] = useState<string>();

  const backendUrl = `${process.env.NEXT_PUBLIC_OOTS_API}/preview`;
  const minimalWebapp = `${process.env.NEXT_PUBLIC_MINIMAL_WEBAPP}`;

  const { data, error } = useSWR<any, unknown>(
    sessionId ? [backendUrl + `/${sessionId}`] : null,
    fetcher,
    {
      refreshInterval: 10010,
    }
  );

  const getVehiclesData = async () => {
    setState(3);

    let reqData = {
      userId: userId,
      previewId: sessionId,
      vehicleIds: vehicleIdsMarked,
    };

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(reqData),
    };

    const res = await fetch(backendUrl, requestOptions);

    if (res.status === 200) {
      const response = await fetch(backendUrl + `/${sessionId}`);
      const jsonData = await response.json();

      setTableData(jsonData.data.vehicleData);
      setState(4);
    }
  };

  useEffect(() => {
    //Fetch Data
    async function fetchData() {
      const payload = { previewId: sessionId };
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ payload }),
      };

      await fetch(backendUrl, requestOptions);
    }

    if (apiData && apiData.data.status === 0) {
      fetchData();
      apiData.data.status = 1;
    }
  }, [apiData, backendUrl, sessionId]);

  useEffect(() => {
    if (sessionId === '' && typeof window !== 'undefined') {
      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);
      const sessionId = urlParams.get('sessionId');
      setSessionId(sessionId);
    }

    if (data && !apiData) {
      setApiData(data);
    }

    if (vehicleIds && state === 1) {
      setState(2);
    }
  }, [apiData, data, sessionId, state, vehicleIds]);

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data: any) => {
    setState(1);
    reset();

    setUserId(data.userId);

    let reqData = {
      userId: data.userId,
      previewId: sessionId,
    };

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(reqData),
    };

    await fetch(backendUrl, requestOptions);

    setTimeout(async () => {
      const response = await fetch(backendUrl + `/${sessionId}`);
      const jsonData = await response.json();

      setVehicleIds(jsonData.data.vehicleIds);
    }, 3000);
  };

  const approveData = async () => {
    setState(5);
    let reqData = {
      userId: userId,
      previewId: sessionId,
      approve: true,
    };

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(reqData),
    };

    const res = await fetch(backendUrl, requestOptions);
    const resData = await res.json();

    if (res.status === 200) {
      setFrontend1URL(minimalWebapp + '/?messageId=' + resData.messageId);
      setTimeout(() => setState(6), 5000);
    }
  };

  return (
    <>
      <Head>
        <title>OOTS EUCARIS Preview Space</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main
        className={clsx(
          styles.container,
          'flex h-full w-full flex-col items-center justify-center pb-40'
        )}
      >
        <h1 className="mb-16 text-5xl">OOTS EUCARIS Preview Space</h1>
        {state === 0 && (
          <>
            <form
              className={clsx(styles.form, 'flex flex-col rounded-lg bg-white')}
              onSubmit={handleSubmit(onSubmit)}
            >
              <div className="flex items-center justify-between rounded-t-lg bg-slate-700 px-7 py-4 text-xl text-white">
                <div>Insert your data</div>
              </div>
              <div className={styles.inputFields}>
                <div
                  className={clsx(
                    styles.inputField,
                    'flex flex-col px-10 py-2 pb-10'
                  )}
                >
                  <label>ID Number</label>
                  <input
                    type="text"
                    className="mt-1 h-8 rounded-lg bg-slate-200 pl-3"
                    {...register('userId', { required: true })}
                  />
                </div>
              </div>
              <div className="flex items-center justify-center rounded-lg">
                <input
                  type="submit"
                  value="Check results"
                  className="mb-8 cursor-pointer rounded-lg bg-green-300 p-2 px-4"
                />
              </div>
            </form>
          </>
        )}
        {state === 1 && (
          <>
            <div>
              <MoonLoader color="#000" />
            </div>
          </>
        )}
        {state === 2 && vehicleIds.length > 0 && (
          <>
            <div className="mb-4 flex items-center justify-between rounded-lg bg-blue-200 px-7 py-4 text-xl text-black">
              <div>Please select which vehicles you want to get data from</div>
            </div>
            <List
              data={vehicleIds}
              setVehicleIds={setVehicleIdsMarked}
              getVehiclesData={getVehiclesData}
            />
          </>
        )}
        {state === 2 && vehicleIds.length === 0 && (
          <>
            <div className="mb-4 flex items-center justify-between rounded-lg bg-blue-200 px-7 py-4 text-xl text-black">
              <div>There are no available vehicles for the introduced data</div>
            </div>
            <div className="mt-10">
              <Button variant="primary" className="bg-green-300">
                <a href={minimalWebapp}>Go back to Procedure portal</a>
              </Button>
            </div>
          </>
        )}
        {state === 3 && (
          <>
            <div>
              <MoonLoader color="#000" />
            </div>
          </>
        )}
        {state === 4 && (
          <>
            <Table data={tableData} />
            <div className="mt-10">
              <Button
                variant="primary"
                className="bg-green-300"
                onClick={approveData}
              >
                Approve Data
              </Button>
            </div>
          </>
        )}
        {state === 5 && (
          <>
            <div>
              <MoonLoader color="#000" />
            </div>
          </>
        )}
        {state === 6 && (
          <>
            <div className="mt-10">
              <Button variant="primary" className="bg-green-300">
                <a href={frontend1URL}>Go back to Procedure portal</a>
              </Button>
            </div>
          </>
        )}
      </main>
    </>
  );
}

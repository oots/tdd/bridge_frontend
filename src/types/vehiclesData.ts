export type VehiclesData = {
  vatResponse: VatResponse;
  aviResponse: AVIResponse[];
};

export type AVIResponse = {
  header: Header;
  body: AVIResponseBody;
};

type AVIResponseBody = {
  VehicleCountries: VehicleCountries;
};

type VehicleCountries = {
  VehicleCountry: VehicleCountry;
};

type VehicleCountry = {
  VehReplyingCountryCode: string;
  VehBodyReplies: VehBodyReplies;
};

type VehBodyReplies = {
  VehBodyReply: VehBodyReply;
};

type VehBodyReply = {
  VehicleRegistrationData: VehicleRegistrationData;
  VehicleTechnicalData: VehicleTechnicalData;
};

type VehicleRegistrationData = {
  VehRegistrationNumber: VehRegistrationNumber;
  VehRegistrationDocDetails: VehRegistrationDocDetails;
  VehFirstRegistrationDateWorld: string;
  VehStartRegistrationDate: string;
};

type VehRegistrationDocDetails = {
  VehRegistrationDocDetail: VehRegistrationDocDetail;
};

type VehRegistrationDocDetail = {
  VehDocumentID: string;
  VehDocIdIssuingAuthority: string;
};

type VehRegistrationNumber = {
  VehRegistrationNumberPart1: string;
};

export type VehicleTechnicalData = {
  VehicleIdentificationNumber: string;
  Type: string;
  Variant: string;
  Version: string;
  VehicleCategoryCode: string;
  TypeApprovalNumber: string;
  NumberOfAxles: string;
  NumberOfWheels: string;
  Wheelbase: string;
  MassOfTheVehicleInRunningOrder: string;
  TechnPermMaxLadenMass: string;
  PrimaryColourCode: string;
  SecondaryColourCode: string;
  NumberOfDoors: string;
  NrOfSeatingPositions: string;
  VehicleFittedWithEcoInnovInd: string;
  FuelTypeCode: string;
  PureElectricVehIndicator: string;
  HybridVehIndicator: string;
  MakeTable: MakeTable;
  CommercialNameTable: CommercialNameTable;
  StageNrOfManufacturingTable: StageNrOfManufacturingTable;
  BodyworkTable: BodyworkTable;
  AxleTable: AxleTable;
  WLTPEmissionTestParamGroup: WLTPEmissionTestParamGroup;
  EngineTable: EngineTable;
  MechanicalCouplingTable: MechanicalCouplingTable;
};

type AxleTable = {
  AxleGroup: AxleGroup[];
};

type AxleGroup = {
  AxleNumber: string;
  AxleTrack: string;
  TechnicallyPermMassAxle: string;
};

type BodyworkTable = {
  BodyworkGroup: BodyworkGroup;
};

type BodyworkGroup = {
  CodeForBodywork: string;
};

type CommercialNameTable = {
  CommercialNameGroup: CommercialNameGroup;
};

type CommercialNameGroup = {
  CommercialName: string;
};

type EngineTable = {
  EngineGroup: EngineGroup;
};

type EngineGroup = {
  EngineCodeAsMarkedOnTheEngine: string;
  EngineNumber: string;
  NumberOfCylinders: string;
  EngineCapacity: string;
  ElectricEngineIndicator: string;
  FuelTable: FuelTable;
};

type FuelTable = {
  FuelGroup: FuelGroup;
};

type FuelGroup = {
  FuelCode: string;
  MaximumNetPower: string;
  EngineSpeedMaximumNetPower: string;
  MaximumSpeed: string;
  SoundLevelStationary: string;
  SoundLevelStatEngineSpeed: string;
  SoundLevelDriveBy: string;
  ExhaustEmissionLevelEuro: string;
  NrBaseRegulActLastAmendMotVeh: string;
  UrbanConditionsCO2: string;
  UrbanConditionsFuelConsumption: string;
  ExtraUrbanConditionsCO2: string;
  ExtraUrbanConditionsFuelCons: string;
  CombinedCO2: string;
  CombinedFuelConsumption: string;
  TestprocedureType1Group: TestprocedureType1Group;
  TestprocedureRdeGroup: TestprocedureRdeGroup;
  TestprocedureWLTPCO2Group: TestprocedureWLTPCO2Group;
  TestprocedureWLTPFuelGroup: TestprocedureWLTPFuelGroup;
};

type TestprocedureRdeGroup = {
  TestprocRdeCompleteRdeNOx: string;
  TestprocRdeCompleteRdeNrOfPart: string;
  TestprocRdeCompleteRdeExpPart: string;
  TestprocRdeUrbanRdeNOx: string;
  TestprocRdeUrbanRdeNrOfPart: string;
  TestprocRdeUrbanRdeExpPart: string;
};

type TestprocedureType1Group = {
  TestprocType1CO: string;
  TestprocType1HC: string;
  TestprocType1NOx: string;
  TestprocType1NMHC: string;
  TestprocType1Particulates: string;
  TestprocType1NrOfParticles: string;
  TestprocType1ExponentParticles: string;
};

type TestprocedureWLTPCO2Group = {
  WLTPLowCO2: string;
  WLTPMediumCO2: string;
  WLTPHighCO2: string;
  WLTPExtraHighCO2: string;
  WLTPCombinedCO2: string;
};

type TestprocedureWLTPFuelGroup = {
  WLTPLowFuelConsumption: string;
  WLTPMediumFuelConsumption: string;
  WLTPHighFuelConsumption: string;
  WLTPExtraHighFuelConsumption: string;
  WLTPCombinedFuelCons: string;
};

type MakeTable = {
  MakeGroup: MakeGroup;
};

type MakeGroup = {
  Make: string;
};

type MechanicalCouplingTable = {
  MechanicalCouplingGroup: MechanicalCouplingGroup;
};

type MechanicalCouplingGroup = {
  TechPermMaxTowMassBrakedTrail: string;
  TechPermMaxTowMassUnbrTrailer: string;
};

type StageNrOfManufacturingTable = {
  StageNrOfManufacturingGroup: StageNrOfManufacturingGroup;
};

type StageNrOfManufacturingGroup = {
  AddressTable: AddressTable;
};

type AddressTable = {
  AddressGroup: AddressGroup;
};

type AddressGroup = {
  Name: string;
};

type WLTPEmissionTestParamGroup = {
  EmisTestMassWLTP: string;
};

type Header = {
  MessageID: string;
  MessageRefID: string;
  MessageVersion: string;
  ServiceExecutionReason: ServiceExecutionReason;
  RecipientCountry: string;
  SenderCountry: string;
  SenderOrganisation: SenderOrganisation;
  SenderName: string;
  TimeStamp: Date;
  TimeOut: string;
};

type SenderOrganisation = {
  SenderOrganisationCode: string;
  SenderOrganisationDesc: string;
};

type ServiceExecutionReason = {
  ServiceExecutionReasonCode: string;
  ServiceExecutionReasonDesc: string;
};

type VatResponse = {
  header: Header;
  body: VatResponseBody;
};

type VatResponseBody = {
  InformationResponse: InformationResponse;
};

type InformationResponse = {
  RequestId: string;
  VehHeldOwnedReferencedateTime: Date;
  VehiclesReply: VehiclesReply;
};

export type VehiclesReply = {
  ListOfVehiclesHeldAndOwned: ListOfVehiclesHeldAndOwned[];
};

type ListOfVehiclesHeldAndOwned = {
  VehicleIdentificationNumber: string;
  VehRegistrationNumber: VehRegistrationNumber;
  VehicleIsHeld: string;
  VehicleIsOwned: string;
};

import clsx from 'clsx';
import styles from './styles.module.scss';

type TableProps = {
  data: any;
};

const Table = ({ data }: TableProps) => {
  return (
    <>
      {data && (
        <table className={clsx('rounded-lg bg-white', styles.table)}>
          <thead>
            <tr className="bg-slate-700 text-white">
              <th>Vehicle Identification Number</th>

              <th>Type</th>
              <th>Vehicle Category Code</th>
              <th>Version</th>
              <th>Brand</th>
            </tr>
          </thead>
          <tbody>
            {data.map((vehicle: any, index: number) => {
              const vehicleTechnicalData: any =
                vehicle.data.body.VehicleCountries.VehicleCountry.VehBodyReplies
                  .VehBodyReply.VehicleTechnicalData;

              return (
                <>
                  <tr key={index}>
                    <td key={index}>{vehicle.vehicleId}</td>
                    <td>{vehicleTechnicalData.Type}</td>
                    <td>{vehicleTechnicalData.VehicleCategoryCode}</td>
                    <td>{vehicleTechnicalData.Version}</td>
                    <td>{vehicleTechnicalData.MakeTable.MakeGroup.Make}</td>
                  </tr>
                </>
              );
            })}
          </tbody>
        </table>
      )}
    </>
  );
};

export default Table;

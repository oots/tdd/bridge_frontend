import { Checkbox } from '@mui/material';
import clsx from 'clsx';
import { useState } from 'react';
import Button from '../Button';
import styles from './styles.module.scss';

type ListProps = {
  data: any;
  setVehicleIds: any;
  getVehiclesData: any;
};

const List = ({ data, setVehicleIds, getVehiclesData }: ListProps) => {
  const [vehiclesIdsList, setVehiclesIdsList] = useState<string[]>([]);
  const handleChange = (vehicle: string) => {
    const arr = vehiclesIdsList.slice();
    if (vehiclesIdsList.includes(vehicle)) {
      arr.splice(vehiclesIdsList.indexOf(vehicle), 1);
      setVehicleIds(arr);
      setVehiclesIdsList(arr);
      return;
    }

    arr.push(vehicle);
    setVehicleIds(arr);
    setVehiclesIdsList(arr);
  };

  return (
    <>
      <table className={clsx('rounded-lg bg-white', styles.list)}>
        <thead>
          <tr className="rounded-lg bg-slate-700 text-white">
            <th>Check</th>
            <th>Vehicle Identification Number</th>
          </tr>
        </thead>
        <tbody>
          {data.map((vehicle: string) => {
            return (
              <>
                <tr>
                  <td>{<Checkbox onChange={() => handleChange(vehicle)} />}</td>
                  <td key={vehicle}>{vehicle}</td>
                </tr>
              </>
            );
          })}
        </tbody>
      </table>
      {vehiclesIdsList && vehiclesIdsList.length > 0 && (
        <div className="mt-10">
          <Button className="bg-white" onClick={getVehiclesData}>
            Get vehicles data
          </Button>
        </div>
      )}
    </>
  );
};

export default List;
